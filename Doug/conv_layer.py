import numpy as np

class ConvLayer:
    """Camada de convolução.

    Todas as imagens e filtros devem ser matrizes bidimensionais quadradas. 

    A primeira dimensão dos conjuntos de imagens e filtros devem conter o número de amostras, 
    enquando as outras duas a dimensão de cada imagem.
    """
    def __init__(self, inputs, filters, padding, stride):
        self.inputs = inputs
        self.filters = filters
        self.num_inputs = int(inputs.shape[0])
        self.num_filters = int(filters.shape[0])
        self.filter_size = int(filters.shape[1])
        self.stride = stride
        self.padding = padding

        self.output_size = int((inputs.shape[1] - self.filter_size + 2*padding)/stride + 1)
    
    def conv_single(self, img, filtro):
    	"""Convoluciona uma imagem para um determinado filtro."""
        if self.padding > 0:
            img_pad = np.pad(array=img, pad_width=self.padding, mode='constant', constant_values=0)
        else:
            img_pad = img
        convoluted = np.zeros((self.output_size, self.output_size))
        
        for i in range(self.output_size):
            for j in range(self.output_size):
                # Geração do campo receptivo por meio de slicing
                img_sub = img_pad[(i*self.stride):(self.filter_size + i*self.stride), (j*self.stride):(self.filter_size + j*self.stride)]
                convoluted[i, j] = np.sum(img_sub*filtro)
        return convoluted
    
    def conv_full(self):
    	"""Convoluciona conjunto de imagens (inputs) com conjunto de filtros."""
        convolutions = np.zeros((self.num_inputs*self.num_filters, self.output_size, self.output_size))
        k = 0
        for i in range(self.num_inputs):
            for j in range(self.num_filters):
                convolutions[k] = self.conv_single(self.inputs[i], self.filters[j])
                k += 1     
        return convolutions
        

    def img2col(self, img):
    	"""Transforma imagem em uma matriz em que cada coluna é um campo receptivo alongado."""
        if self.padding > 0:
            img_pad = np.pad(array=img, pad_width=self.padding, mode='constant', constant_values=0)
        else:
            img_pad = img
        img_col = np.zeros((self.filter_size**2, self.output_size**2))
        k = 0
        
        for i in range(self.output_size):
            for j in range(self.output_size):
                # Gera campo receptivo através de slicing, achatando-o logo em seguida.
                img_col[:, k] = np.ravel(img_pad[(i*self.stride):(self.filter_size + i*self.stride), (j*self.stride):(self.filter_size + j*self.stride)])
                k += 1        
        return img_col
    
    
    def conv_dot(self):
    	"""Método de multiplicação matricial para convolução.

    	Utiliza img2col para multiplicar matriz de filtros (esticados em linhas)
    	com uma matriz de campos receptivos (estiados em colunas).
    	Performance é superior à convolução completa implementada acima.
    	"""
        W_row = np.reshape(self.filters, (self.num_filters, self.filter_size**2))
        output_dot = np.zeros((self.num_inputs, self.filter_size, self.output_size**2))
        
        for i in range(self.num_inputs):
            output_dot[i] = np.dot(W_row, self.img2col(self.inputs[i]))
        
        # Retornando resultado com suas dimensões originais
        return np.reshape(output_dot, (self.num_inputs*self.num_filters, self.output_size, self.output_size))