import numpy as np


class FC:
    """Incompleta."""
    def __init__(self):
        # Hiperparâmetros
        self.inputLayerSize = 10
        self.outputLayerSize = 10
        self.h1LayerSize = 20
        self.h2LayerSize = 20
        
        # Pesos
        self.W1 = np.random.randn(self.inputLayerSize, self.h1LayerSize)
        self.W2 = np.random.randn(self.h1LayerSize, self.h2LayerSize)
        self.W3 = np.random.randn(self.h2LayerSize, self.outputLayerSize)
    
    def feedforward(self, X):
        # Propagação para a primeira camada oculta
        self.z1 = np.dot(X, W1)
        self.a2 = self.sigmoid(self.z1)

        #Propagação para a segunda camada oculta
        self.z2 = np.dot(self.a2, self.W2)
        self.a3 = self.sigmoid(self.z2)

        # Propagação para a camada de saída
        self.z3 = np.dot(self.a3, self.W3)
        yHat = self.sigmoid(self.z3)
        return yHat

    def sigmoid(self, z):
        # Função de ativação
        return 1/(1 + np.exp(-z))

    def sigmoid_prime(self, z):
        # Derivada da função de ativação
        return -z/((1 + np.exp(-z))**2)