import numpy as np
import time

########## Classes ############

###########################################################################################

class CNN:
	def __init__(self):
		self.model = []

	def addConvolutional(self, shape, num, rate=0.7):
		self.model.append( Convolutional(shape, num, rate) )

	def addFC(self, list_neu, rate=0.7):
		self.model.append( FC(list_neu, rate) )

	def FeedFoward(self, images_array):
		entrada = images_array.copy()
		for i in range(len(self.model)):
			entrada = self.model[i].FeedFoward(entrada)
		return entrada

	def BackPropagation(self, images_array, output):
		entrada = []
		entrada.append(images_array.copy())
		for i in range(len(self.model)):
			entrada.append(self.model[i].FeedFoward(entrada[i]))

		grad = self.model[-1].BackPropagation(output)
		for i in range(1,len(self.model)):
			grad = self.model[len(self.model) - 1 - i].BackPropagation(grad, entrada[len(self.model)- 1 - i])

	def Treino(self, images_array, output, i_max = 200):
		for i in range(i_max):
			n = np.random.randint(images_array.shape[0])
			entrada = np.array([images_array[n]])
			classe = np.array([output[n]])
			print( np.abs(self.FeedFoward(entrada)-classe))
			self.BackPropagation(entrada, classe)


###########################################################################################

class Filtro:
	def __init__(self, shape, rate):
		self.shape = shape
		self.rate = rate
		self.filtro = np.random.random(shape)

	def set_filter(self, filter_):
		self.filtro = filter_
		self.shape = filter_.shape

	def run(self, imagem):
		l,c = self.shape
		i_max, j_max = imagem.shape
		new_image = np.ones((max(1,i_max-l), max(1,j_max-c)))
		for i in range(i_max - l):
			for j in range(j_max - c):
				new_image[i,j] = np.sum(np.multiply(imagem[i:i+l, j:j+c], self.filtro))
		return new_image

	def AdaptFilter(self, grad):
		self.filtro += grad


###########################################################################################

class Convolutional:
	def __init__(self, shape, num, rate):
		self.num = num
		self.shape = shape
		self.rate = rate
		self.new = True

	def CreateFilters(self, dim):
		filtros = []
		for i in range(self.num*dim):
			filtros.append(Filtro( self.shape , self.rate))
		self.filtros = np.array(filtros)
		self.new = False


	def FeedFoward(self, images_array):
		dim = images_array.shape[0]
		if self.new:
			self.CreateFilters(dim)
		conv = []
		for i in range(self.num):
			for j in range(dim):
				conv.append( self.filtros[i*dim + j].run(images_array[j]) )
		return np.array(conv)

	def BackPropagation(self, grad, input_):
		if len(grad.shape) == 3:
			grad_filter = {}
			for i in range(grad.shape[0]):
				filtro = Filtro( self.shape , self.rate)
				filtro.set_filter(grad[i])
				grad_filter[i] = filtro.run(input_[i])

			for i in range(grad.shape[0]):
				self.filtros[i].AdaptFilter(grad_filter[i])


		else:
			grad_filter = {}
			filtro = Filtro( self.shape , self.rate).set_filter(grad)
			self.filtros.AdaptFilter(filtro.run(input_))

		grad_img = np.pad(grad, ((0,0),(1,1),(1,1)), 'constant', constant_values=0)
		return self.FeedFoward(grad_img)

		
#########################################################################################

class FC:
	def __init__(self, list_neu, rate):
		self.list_neu = list_neu
		self.rate = rate
		self.new = True

	def CreateWeights(self, input_):
		pesos = {}
		pesos[0] = np.random.random((len(input_)+1, self.list_neu[0]))
		for i in range(1, len(self.list_neu)):
			pesos[i] = np.random.random((self.list_neu[i-1]+1, self.list_neu[i]))
		self.pesos = pesos
		self.new = False

	def FeedFoward(self, input_):
		self.input_shape = input_.shape
		input_ = input_.copy().flatten()
		layers = len(self.list_neu)
		self.input = {}
		if self.new:
			self.CreateWeights(input_)
		for i in range(layers):
			input_ = np.append(1,input_)
			self.input[i] = input_.copy()
			input_ = np.tanh(np.dot(input_, self.pesos[i]))
		self.output = input_.copy()
		return input_

	def BackPropagation(self, output):
		layers = len(self.list_neu)
		grad = {}
		grad[layers-1] = (self.output-output)*(1-np.tanh(np.dot(self.input[layers-1],self.pesos[layers-1]))**2)
		for i in range(1, layers):
			grad_n = (self.pesos[layers-i][1:]*grad[layers-i]).sum(axis=1)
			grad[layers-1-i] = grad_n*(1-np.tanh(np.dot(self.input[layers-1-i],self.pesos[layers-1-i]))**2)
		self.grad = grad	

		for i in range(layers):
			self.pesos[i] -= self.rate*(self.input[i].reshape((-1,1))*self.grad[i])

		return (self.pesos[0][1:]*self.grad[0]).sum(axis=1).reshape(self.input_shape)

#########################################################################################